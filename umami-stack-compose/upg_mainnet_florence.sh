#!/bin/bash

# sudo mkdir /var/log/umami-backend/
# sudo chown tezos:tezos /var/log/umami-backend/
# sudo su tezos 
# crontab -e

#run every minute from 10/may to 12/may 
# * * 10-12 5 * /opt/umami-backend/amino/config/upg_mainnet_florence.sh >> /var/log/umami-backend/upg_mainet_florence 2>&1

# tail -f /var/log/umami-backend/upg_mainet_florence

echo "`date` - $0 -- start"
for env in mainnet2 mainnet 
do 
	echo "`date` - $0 - $env"

	make -f /opt/umami-backend/amino/${env}/Makefile -C /opt/umami-backend/amino/${env}/ upg_mezos_florence 2>&1
	sudo docker inspect ${env}_mezos_1 | grep MEZOS_PROTO

done
echo "`date` - $0 -- end"
