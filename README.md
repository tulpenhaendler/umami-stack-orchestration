# Umami Stack Orchestration

Umami client needs a backend to access tezos blockchain data.

This project orchestrate the following components :
- octez tezos-node : connection with the tezos blockchain
- tezos-indexer : indexing data from the node to a database
- postgres 
- mezos : API interface for the indexer
- and prometheus exporters to run around.


| component                                                           | url | latest release |
| ---                | ---                                            | ---|
| tezos-node (octez) | https://gitlab.com/tezos/tezos/                | [![releases-octez](https://badgen.net/gitlab/release/tezos/tezos)](https://gitlab.com/tezos/tezos/-/releases) |
| tezos-indexer      | https://gitlab.com/nomadic-labs/tezos-indexer  | [![releases](https://badgen.net/gitlab/release/nomadic-labs/tezos-indexer)](https://gitlab.com/nomadic-labs/tezos-indexer/-/releases)   |
| postgres           |                                                | |
| mezos              | https://gitlab.com/nomadic-labs/mezos          | [![releases](https://badgen.net/gitlab/release/nomadic-labs/mezos)](https://gitlab.com/nomadic-labs/mezos/-/releases) | 
| umami              | https://gitlab.com/nomadic-labs/umami-wallet/umami          | [![releases](https://badgen.net/gitlab/release/nomadic-labs%2Fumami-wallet/umami)](https://gitlab.com/nomadic-labs/umami-wallet/umami/-/releases) | 







### docker-compose
* `docker-compose.yml` defines the orchestration of the different services running together.
* All environment variables are configured in the `.env` file, by default for mainnet.
* The Makefile defines a set of helpers

![Orchestration Docker-compose de Umami Backend pour 1 net (sans monitoring)](./images/docker-compose.png "Orchestration Docker-compose de Umami Backend pour 1 net (sans monitoring)")




